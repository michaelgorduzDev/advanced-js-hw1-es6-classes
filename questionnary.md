Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Every time when class B inherits prototype class A, B also inherits all properties of a prototype
A. In addition, A can inherits from C, and C can inherits from D. This calls prototype chain.
Expample: If class A have color:white and class B inherits from A and class C inherits from B, 
all those classes will have color:white unless color will change implicitly.



Для чого потрібно викликати super() у конструкторі класу-нащадка?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
We need to call parent constructor because otherwise this.object will not be created. After
calling super() we will have parent class fields applied to our extended class.