class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }

    set name(value) {
        this._name = value;
    }

    get age() {
        return this._age;
    }

    set age(value) {
        this._age = value;
    }

    get salary() {
        return this._salary;
    }

    set salary(value) {
        this._salary = value;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }

    set lang(value) {
        this._lang = value;
    }

    get lang() {
        return this._lang;
    }

    get salary() {
        return this._salary * 3;
    }

}

const john = new Programmer("John", 28, 1000, ["JavaScript", "Python"]);
const mary = new Programmer("Mary", 32, 500, ["Java", "C++"]);

console.log(john);
console.log(mary);
